package com.brain.api;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class AnnonceControllerTest {
    
    @Autowired
	public MockMvc mockMvc;

	@Test
	public void testGetAnnonce() throws Exception {
		
		mockMvc.perform(get("/annonces")).andExpect(status().isOk()).andExpect(jsonPath("$[0].title", is("immobilier")));
		
	}
     
    @Test
    public void getAnnonceById() throws Exception 
    {
        mockMvc.perform( MockMvcRequestBuilders
        .get("/annonce/{id}", "903117ef-f8f2-4c5b-b4f3-9dc80b63ebc2")
        .accept(MediaType.APPLICATION_JSON))
        .andDo(MockMvcResultHandlers.print())
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("903117ef-f8f2-4c5b-b4f3-9dc80b63ebc2"));
    }

}