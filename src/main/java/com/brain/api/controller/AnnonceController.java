package com.brain.api.controller;

import java.util.Optional;
import java.util.UUID;

import com.brain.api.model.Annonce;
import com.brain.api.service.AnnonceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnnonceController {

    @Autowired
	private AnnonceService annonceService;
	
	/**
	 * Create - Add a new annonce
	 * @param annonce An object annonce
	 * @return The annonce object saved
	 */
	@PostMapping("/annonce")
	public Annonce createAnnonce(@RequestBody Annonce annonce) {
		return annonceService.saveAnnonce(annonce);
	}
	
	
	/**
	 * Read - Get one annonce 
	 * @param id The id of the annonce
	 * @return An annonce object full filled
	 */
	@GetMapping("/annonce/{id}")
	public Annonce getAnnonce(@PathVariable("id") final UUID id) {
		Optional<Annonce> annonce = annonceService.getAnnonce(id);
		if(annonce.isPresent()) {
			return annonce.get();
		} else {
			return null;
		}
	}
	
	/**
	 * Read - Get all annonce
	 * @return - An Iterable object of Annonce full filled
	 */
	@GetMapping("/annonces")
	public Iterable<Annonce> getAnnonce() {
		return annonceService.getAnnonce();
	}
	
	/**
	 * Update - Update an existing annonce
	 * @param id - The id of the annonce to update
	 * @param annonce - The annonce object updated
	 * @return
	 */
	@PutMapping("/annonce/{id}")
	public Annonce updateAnnonce(@PathVariable("id") final UUID id, @RequestBody Annonce annonce) {
		Optional<Annonce> a = annonceService.getAnnonce(id);
		if(a.isPresent()) {
			Annonce currentAnnonce = a.get();
			
			String title = annonce.getTitle();
			if(title != null) {
				currentAnnonce.setTitle(title);
			}
			String description = annonce.getDescription();
			if(description != null) {
				currentAnnonce.setDescription(description);
			}
			String picture = annonce.getPicture();
			if(picture != null) {
				currentAnnonce.setPicture(picture);
			}
			annonceService.saveAnnonce(annonce);
			return currentAnnonce;
		} else {
			return null;
		}
	}
	
	
	/**
	 * Delete - Delete an annonce
	 * @param id - The id of the annonce to delete
	 */
	@DeleteMapping("/annonce/{id}")
	public void deleteAnnonce(@PathVariable("id") final UUID id) {
		annonceService.deleteAnnonce(id);
	}
	
    
}
