package com.brain.api.repository;

import java.util.UUID;

import com.brain.api.model.Annonce;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnnonceRepository extends CrudRepository<Annonce,UUID>{
    
}
