package com.brain.api.service;

import java.util.Optional;
import java.util.UUID;

import com.brain.api.model.Annonce;
import com.brain.api.repository.AnnonceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnnonceService {
    
    @Autowired
	private AnnonceRepository annonceRepository;
	
	public Optional<Annonce> getAnnonce(final UUID id) {
		return annonceRepository.findById(id);
	}
	
	public Iterable<Annonce> getAnnonce() {
		return annonceRepository.findAll();
	}
	
	public void deleteAnnonce(final UUID id) {
		annonceRepository.deleteById(id);
	}
	
	public Annonce saveAnnonce(Annonce annonce) {
		return annonceRepository.save(annonce);
	}
}
